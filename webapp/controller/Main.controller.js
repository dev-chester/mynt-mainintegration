sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/model/json/JSONModel",
	"sap/m/Dialog",
	"sap/m/Label",
	"sap/m/TextArea",
	"sap/m/Button",
	"sap/m/ButtonType",
	"sap/m/MessageToast"

], function (Controller, JSONModel, Dialog, Label, TextArea, Button, ButtonType, MessageToast) {
	"use strict";


	return Controller.extend("com.apptech.app-mainintegration.controller.Main", {

		onInit: function () {
			this.oMdlLog = new JSONModel("model/log.json");
			this.getView().setModel(this.oMdlLog, "oMdlLog");
			this.oFromDate = {};
			this.oBtnJE = this.getView().byId("btnJE");
		},

		onSyncJE: function (oEvent) {
			// this.oBtnJE.setBusy(true);

			console.log("-----Starting to integrate Journal Entries-----");
			this.onLogMsg("-----Starting to integrate Journal Entries-----");
			// this.oFromDate.fromDate = "20200116";
			var filename = "transaction_summary_" + this.oFromDate.fromDate + ".csv";

			var aS3StringContent = "";

			var settings = {
				"url": "http://localhost:3002/j1",
				"method": "POST",
				"timeout": 0,
				"async": false,
				"headers": {
					"Content-Type": "application/json"
				},
				"context": this,
				"data": JSON.stringify({
					"fromDate": "20200116"
				}),
			};
			console.log("-----Downloading Files from S3 Bucket-----");
			this.onLogMsg("-----Downloading Files from S3 Bucket-----");
			$.ajax(settings).done(function (response) {
				aS3StringContent = response;
			});

			var oneRow = {
				wallet_name: "",
				transtype: "",
				fee: "",
				transaction_date: "",
				transaction_count: "",
				transaction_amount: "",
				reversal_count: "",
				reversal_amount: "",
				net_count: "",
				net_amount: "",
				receivable_amount: "",
				tax_code: "",
				revenue_code: "",
				currency: ""
			};

			var allRows = [];

			aS3StringContent.forEach(function (value, index) {
				if (index === 0) {

				} else {

					var aValues = value.split(',');

					oneRow.wallet_name = aValues[0];
					oneRow.transtype = aValues[1];
					oneRow.fee = aValues[2];
					oneRow.transaction_date = aValues[3];
					oneRow.transaction_count = aValues[4];
					oneRow.transaction_amount = aValues[5];
					oneRow.reversal_count = aValues[6];
					oneRow.reversal_amount = aValues[7];
					oneRow.net_count = aValues[8];
					oneRow.net_amount = aValues[9];
					oneRow.receivable_amount = aValues[10];
					oneRow.tax_code = aValues[11];
					oneRow.revenue_code = aValues[12];
					oneRow.currency = aValues[13];

					allRows.push(JSON.parse(JSON.stringify(oneRow)));

				}
			});


			console.log("-----Filtering Downloaded data-----");
			this.onLogMsg("-----Filtering Downloaded data-----");
			//FILTER PER CURRENCY
			var aAllPHPRows = allRows.filter(function (e) {
				if ((typeof e.currency === 'string' || e.currency instanceof String) &&
					e.fee.toString().charAt(0) !== "0" &&
					e.transaction_count !== "0" &&
					e.currency.match(/PHP.*/)) {
					return true;
				}
			});

			console.log("-----Constructing Object for Journal Entries-----");
			this.onLogMsg("-----Constructing Object for Journal Entries-----");


			var aConstructedObject = this.constructJEObject(aAllPHPRows, "PHP", filename, this.oFromDate.fromDate);

			console.log("finished constructJEObject");
			//Fetch Token
			var sToken = "";
			settings = {
				"url": "http://localhost:3002/jt",
				"method": "GET",
				"timeout": 0,
				"async": false,
				"headers": {
					"Content-Type": "application/json"
				}
			}; 
			$.ajax(settings).done(function (response) {
				sToken = response;
			});


			// // var sToken = this.getToken();

			// aConstructedObject.forEach( (i) => { 
			//     settings = {
			//         "url": "http://localhost:3002/j2?t=" + sToken,
			//         "method": "POST",
			//         "timeout": 0,
			//         "async": false,
			//         "headers": {
			//             "Content-Type": "application/json"
			//         },
			//         "data": JSON.stringify(i)
			//     }; 
			//     $.ajax(settings).done(function (response) {
			//         sToken = response;
			//     });
			//     // this.postJournalEntry(JSON.stringify(i), token);
			// });

			//  this.oBtnJE.setBusy(false);


		},

		onPromptDate: function () {
			var oThat = this;
			var oTextArea = new TextArea('submitDialogTextarea', {
				liveChange: function (oEvent) {
					var sText = oEvent.getParameter('value');
					var parent = oEvent.getSource().getParent();

					parent.getBeginButton().setEnabled(sText.length > 0);
				},
				width: '100%',
				placeholder: '(required) YYYYMMDD'
			});

			var oDialog = new Dialog({
				title: 'Confirm',
				type: 'Message',
				content: [
					new Label({
						text: 'Enter Date to be synced (Format YYYYMMDD) : ',
						labelFor: 'submitDialogTextarea'
					}),
					oTextArea
				],
				beginButton: new Button({
					type: ButtonType.Emphasized,
					text: 'Sync JE',
					enabled: false,
					press: function () {
						var sText = sap.ui.getCore().byId('submitDialogTextarea').getValue();
						oDialog.close();
					}
				}),
				endButton: new Button({
					text: 'Cancel',
					press: function () {
						oDialog.close();
					}
				}),
				afterClose: () => {
					console.log(oTextArea);
					this.oFromDate.fromDate = oTextArea._lastValue;

					oDialog.destroy();
					this.onSyncJE(1);
				}
			});

			oDialog.open();
		},

		//return Array of objects with header and details
		constructJEObject: function (aFilteredRows, sCurrency, sFileName, sDate) {

			var oThat = this;
			var fileName = sFileName;
			var aJournalEntry = [];

			var oJE = {
				CompanyUUID: '00163E93-9B49-1EEA-8387-F043FB7736D7',
				AccountingDocumentTypeCode: "00103",
				BusinessTransactionTypeCode: "601",
				Note: "from NodeJS",
				OffsettingPostingDate: "/Date(1580428800000)/",
				PostingDate: "/Date(1579132800000)/",
				AccountingEntryItem: []
			};

			if (aFilteredRows.length === 0) {
				return undefined;
			}

			var hasError = false;

			var aUnique = [];
			var aUniqueWallet = [];

			//CREATE A WEB WORKER FOR MULTI THREADING
			var workerCreateJEObject = new Worker("../WebWorker.js");
			workerCreateJEObject.postMessage(aFilteredRows);

			workerCreateJEObject.onmessage = (event) => {
				console.log(event.data.glvat);
				console.log(event.data.pcupc);

				var oJE = {
					CompanyUUID: '00163E93-9B49-1EEA-8387-F043FB7736D7',
					AccountingDocumentTypeCode: "00103",
					BusinessTransactionTypeCode: "601",
					Note: "from NodeJS",
					OffsettingPostingDate: "/Date(1580428800000)/",
					PostingDate: "/Date(1579132800000)/",
					AccountingEntryItem: []
				};

				var iCount = 1;
				var iBatch = 1;
				var rowCount = aFilteredRows.length;
				aFilteredRows.forEach((element) => {
					var sTransType_fee = element.transtype.toUpperCase() + "_" + element.fee;
					var sProfitCenter = event.data.pcupc.find(y => y.tranType === sTransType_fee).profitCenter;
					var sUUIDProfitCenter = event.data.pcupc.find(y => y.tranType === sTransType_fee).profitUUID;
					var sGLAccount = event.data.glvat.find(z => z.wallet_name === element.wallet_name).gl_account;
					var sVATDetail = event.data.glvat.find(w => w.wallet_name === element.wallet_name).vat_detail;

					//CALCULATIONS
					var dFee = parseFloat(element.fee);
					var dTransactionCount = parseFloat(element.transaction_count);
					var dNetAmount = dFee * dTransactionCount;
					var dNetAmountLessVAT = (dNetAmount - (dNetAmount * 0.12)) * -1;
					var dNetVAT = (dNetAmount * 0.12) * -1;
					var sNetAmountLessVAT = dNetAmountLessVAT.toFixed(2);
					var sNetVAT = dNetVAT.toFixed(2);

					if (sVATDetail === "VATA") {
						var oDebit = {
							ChartOfAccountsCode: "CAPH",
							ChartOfAccountsItemCode: "129999",
							DebitCreditCode: "1",
							TransactionCurrencyAmount: (dNetAmount + "").replace(",", ""),
							currencyCode1: sCurrency,
							ProfitCentreUUID: sUUIDProfitCenter,
							//SegmentUUID: sUUIDProfitCenter,
							Note: element.wallet_name,
							ServiceProduct_KUT: sTransType_fee,
							TransactionCount_KUT: element.transaction_count,
						};

						var oCredit = {
							ChartOfAccountsCode: "CAPH",
							ChartOfAccountsItemCode: sGLAccount,
							DebitCreditCode: "2",
							TransactionCurrencyAmount: sNetAmountLessVAT + "",
							currencyCode1: sCurrency,
							ProfitCentreUUID: sUUIDProfitCenter,
							//SegmentUUID: sUUIDProfitCenter,
							Note: element.wallet_name,
							ServiceProduct_KUT: sTransType_fee,
							TransactionCount_KUT: element.transaction_count
						};

						var oCreditVAT = {
							ChartOfAccountsCode: "CAPH",
							ChartOfAccountsItemCode: "230127",
							DebitCreditCode: "2",
							TransactionCurrencyAmount: sNetVAT,
							currencyCode1: sCurrency,
							ProfitCentreUUID: sUUIDProfitCenter,
							//SegmentUUID: sUUIDProfitCenter,
							Note: element.wallet_name,
							ServiceProduct_KUT: sTransType_fee,
							TransactionCount_KUT: element.transaction_count
						};

						oJE.AccountingEntryItem.push(JSON.parse(JSON.stringify(oDebit)));
						oJE.AccountingEntryItem.push(JSON.parse(JSON.stringify(oCredit)));
						oJE.AccountingEntryItem.push(JSON.parse(JSON.stringify(oCreditVAT)));

					} else {

						var oDebit = {
							ChartOfAccountsCode: "CAPH",
							ChartOfAccountsItemCode: "129999",
							DebitCreditCode: "1",
							TransactionCurrencyAmount: (dNetAmount + "").replace(",", "") + "",
							currencyCode1: sCurrency,
							ProfitCentreUUID: sUUIDProfitCenter,
							//SegmentUUID: sUUIDProfitCenter,
							Note: element.wallet_name,
							ServiceProduct_KUT: sTransType_fee,
							TransactionCount_KUT: element.transaction_count
						};

						var dNetAmountNegative = dNetAmount * -1;
						var oCredit = {
							ChartOfAccountsCode: "CAPH",
							ChartOfAccountsItemCode: sGLAccount,
							DebitCreditCode: "2",
							TransactionCurrencyAmount: dNetAmountNegative + "",
							currencyCode1: sCurrency,
							ProfitCentreUUID: sUUIDProfitCenter,
							//SegmentUUID: sUUIDProfitCenter,
							Note: element.wallet_name,
							ServiceProduct_KUT: sTransType_fee,
							TransactionCount_KUT: element.transaction_count
						};
						oJE.AccountingEntryItem.push(JSON.parse(JSON.stringify(oDebit)));
						oJE.AccountingEntryItem.push(JSON.parse(JSON.stringify(oCredit)));

					}

					if (iCount % 50 === 0) {
						oJE.Note = fileName + "-" + iBatch;
						// IF FINISH ALL
						if (iCount === rowCount) {
							aJournalEntry.push(JSON.parse(JSON.stringify(oJE)));
							return aJournalEntry;
						}
						console.log(JSON.stringify(oJE));
						aJournalEntry.push(JSON.parse(JSON.stringify(oJE)));
						oJE = {};
						oJE.CompanyUUID = "00163E93-9B49-1EEA-8387-F043FB7736D7";
						oJE.AccountingDocumentTypeCode = "00103";
						oJE.BusinessTransactionTypeCode = "601";
						oJE.OffsettingPostingDate = "/Date(1580428800000)/";
						oJE.PostingDate = "/Date(1579132800000)/";
						oJE.Note = "";
						oJE.AccountingEntryItem = [];
						iBatch++;
					} else {
						if (iCount === rowCount) {
							aJournalEntry.push(JSON.parse(JSON.stringify(oJE)));
							return aJournalEntry;
						}
					}
					iCount++;
				})
				console.log("finished forEach");
				return aJournalEntry;
			};
 
		},
		onLogMsg: function (sMessage) {
			this.oMdlLog.getData().logs = sMessage + "\n" + this.oMdlLog.getData().logs;
			this.oMdlLog.refresh();
		}
	})

})
