sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/m/MessageToast",
	"sap/ui/model/json/JSONModel"
], function (Controller, MessageToast, JSONModel) {
	"use strict";

	return Controller.extend("com.apptech.app-mainintegration.controller.Login", {
		onInit: function () {

		},

		onLogin: function (oEvent) {
			//   sap.ui.core.UIComponent.getRouterFor(this).navTo("BankInteg");
			var passCode = this.getView().byId("inputPass").getValue();
			passCode = encodeURIComponent(passCode);
			var loginURL = "http://localhost:3002/ExecQuery?spname=spAppBankIntegration&queryTag=logingetrow&value1=" + passCode + "&value2&value3&value4";
			$.ajax({
				url: loginURL,
				type: "GET",
				error: function (xhr, status, error) {
					console.log("error ");
				},
				success: function (json) {
					console.log("Connected");
				},
				context: this
			}).done(function (results) {
				if (results.length == 0) {
					MessageToast.show("Please enter valid passcode");
				} else {
					var oMdlRole = new JSONModel();
					oMdlRole.getData().Role = results[0].Role;
					// oMdlRole.setJSON(JSON.parse(JSON.stringify(results).replace("[","").replace("]","")));
					//JSON.parse(JSON.stringify(results).replace("[","").replace("]",""));
					sap.ui.getCore().setModel(oMdlRole, "oMdlRole");
					sap.ui.core.UIComponent.getRouterFor(this).navTo("Main");
				}
			});
		}


	});
});
