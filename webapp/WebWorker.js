self.onmessage = (event) => {
	console.log(`Total Distinct Account: ${event.data.length}`);

	var aNotExistWallet = [];

	var aUnique = [];
	var aUniqueWallet = [];
	var xhr = new XMLHttpRequest();
	var xhr2 = new XMLHttpRequest();
	for (var x = 0; x < event.data.length; x++) {

		var isExistReturnVal = "";

		xhr.open("GET", "http://localhost:3002/jexist?walletname=" + event.data[x].wallet_name.toString().toUpperCase(), false);
		xhr.send();

		xhr.addEventListener("readystatechange", function () {
			if (this.readyState === 4) {
				if (JSON.parse(this.responseText).c === "0") {
					aNotExistWallet.push(JSON.parse(this.responseText).w);
				}
			}
		});

	}

	if (aNotExistWallet.length !== 0) {
		self.postMessage({
			"resultCode": "-1",
			"resultMsg": "Cannot proceed to posting with Non Existing wallet name",
			"resultObj": aNotExistWallet
		});
		self.close();
	} //END OF WEB WORKER IF LENGTH THERES A NON EXISTING WALLET


	let glvatloop = async () => {
		//https://stackoverflow.com/questions/30008114/how-do-i-promisify-native-xhr
		for (var y = 0; y < event.data.length; y++) {

			makeRequest('GET', 'http://localhost:3002/jglvt?walletname=' + event.data[y].wallet_name)
				.then(function (d) {
					var oRecordWallet = JSON.parse(d);

					aUniqueWallet.push({
						'wallet_name': oRecordWallet.wallet_name,
						'gl_account': oRecordWallet.gl,
						'vat_detail': oRecordWallet.vat
					});
				})
				.catch(function (err) {
					console.error('Augh, there was an error!', err.statusText);
				});

			//=======================


		}
	}

	let pcupcloop = async () => {
		for (var y = 0; y < event.data.length; y++) {
			var sTransTypeFee = event.data[y].transtype.toString().toUpperCase() + "_" + event.data[y].fee;
			var isFound = aUnique.findIndex(x => x.tranType === sTransTypeFee);

			if (isFound === -1) {
                console.log("No found: " + sTransTypeFee);
				var sProfitCenter = "";

				await makeRequest('GET', 'http://localhost:3002/jpc?transtypefee=' + sTransTypeFee)
					.then((d) => {
                        var sProfitCenter = JSON.parse(d).p;
                        console.log("profitcenter for " + sTransTypeFee + " is " + sProfitCenter);
						return makeRequest('GET', 'http://localhost:3002/jupc?profitcenter=' + sProfitCenter);
					})
					.then((e) => {
                        var sUUIDProfit = JSON.parse(e).u;
                        var sProfCenter = JSON.parse(e).p;
						aUnique.push({
							"tranType": sTransTypeFee,
							"profitCenter": sProfCenter,
							"profitUUID": sUUIDProfit
						});
					})
					.catch(function (err) {
						console.error('Augh, there was an error!', err.statusText);
					});



				// xhr.open("GET", "http://localhost:3002/jpc?transtypefee=" + sTransTypeFee, false);
				// xhr.send();

				// xhr.addEventListener("readystatechange", function () {
				// 	if (this.readyState === 4) {
				// 		var sProfitCenter = JSON.parse(this.responseText).p;

				// 		xhr.open("GET", "http://localhost:3002/jupc?profitcenter=" + sProfitCenter, false);
				// 		xhr.send();

				// 		xhr.addEventListener("readystatechange", () => {
				// 			if (this.readyState === 4) {
				// 				var sUUIDProfit = JSON.parse(this.responseText).u;
				// 				aUnique.push({
				// 					"tranType": sTransTypeFee,
				// 					"profitCenter": sProfitCenter,
				// 					"profitUUID": sUUIDProfit
				// 				});

				// 			}
				// 		});

				// 	}
				// });


			} else {
				continue;
			}
		}
	}

	glvatloop().then((a) => {
			console.log("unitwallet"); 
    });

    pcupcloop().then((b) => {
        console.log("unique"); 
        self.postMessage({"glvat":aUniqueWallet, "pcupc":aUnique});
    });
    
    



}

function makeRequest(method, url, done) {
	return new Promise(function (resolve, reject) {
		var xhr = new XMLHttpRequest();
		xhr.open(method, url);
		xhr.onload = function () {
			if (this.status >= 200 && this.status < 300) {
				resolve(xhr.response);
			} else {
				reject({
					status: this.status,
					statusText: xhr.statusText
				});
			}
		};
		xhr.onerror = function () {
			reject({
				status: this.status,
				statusText: xhr.statusText
			});
		};
		xhr.send();
	});
}
